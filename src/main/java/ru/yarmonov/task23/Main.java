package ru.yarmonov.task23;

/**
 * Реализовать класс корзины интернет магазина по следующему интерфейсу:
 *
 * interface Basket {
 *
 *     void addProduct(String product, int quantity);
 *
 *     void removeProduct(String product);
 *
 *     void updateProductQuantity(String product, int quantity);
 *
 *     void clear();
 *
 *     List<String> getProducts();
 *
 *     int getProductQuantity(String product);
 *
 * }
 *
 */

public class Main {

    public static void main(String[] args) {

        MyBasket myBasket = new MyBasket();

        myBasket.addProduct("aaa", 4);
        myBasket.addProduct("bbb", 6);
        myBasket.addProduct("ccc", 7);

        for (String product : myBasket.getProducts()) {
            System.out.println(product + " " + myBasket.getProductQuantity(product));
        }

        myBasket.removeProduct("bbb");
        myBasket.updateProductQuantity("aaa", 5);

        for (String product : myBasket.getProducts()) {
            System.out.println(product + " " + myBasket.getProductQuantity(product));
        }


    }

}
