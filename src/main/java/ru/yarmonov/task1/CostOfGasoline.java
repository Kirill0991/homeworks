package ru.yarmonov.task1;

public class CostOfGasoline {

    // программа, которая считает стоимость бензина (на вход программе передается кол-во литров, на выходе печатается стоимость).
    // Пример: стоимость литра бензина 43 рубля. На вход подается 50, на выходе должно быть 2150 руб.

    public static void main(String[] args) {

        // первый вариант реализации
        int priseGasoline = 50;
        int costGasoline = 43;
        int totalSum = priseGasoline * costGasoline;
        System.out.println(totalSum);

        // второй вариант реализации с входными параметрами
        int liteParameter = Integer.parseInt(args[0]);
        int totalCost = liteParameter * costGasoline;
        System.out.println(totalCost);

    }

}