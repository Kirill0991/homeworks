package ru.yarmonov.task1;

public class HandSalary {

    //    программа, которая считает зарплату «на руки» (на вход программе передается величина зарплаты, на выходе печатается зарплата за вычетом 13% (НДФЛ).
    //    Пример: на вход подается 70000, на выходе печатается 60900 руб.

    public static void main(String[] args) {

        // первый вариант реализации
        int salary = 70000;
        int percent = 13;
        int totalSum = (salary * percent) / 100;
        int totalHandSalary = salary - totalSum;
        System.out.println(totalHandSalary);

        // второй вариант реализации с входными параметрами
        int salaryParameter = Integer.parseInt(args[0]);
        int totalSumParameter = (salaryParameter * percent) / 100;
        int totalHandSalaryParameter = salaryParameter - totalSumParameter;
        System.out.println(totalHandSalaryParameter);

    }

}