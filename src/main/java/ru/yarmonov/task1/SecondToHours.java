package ru.yarmonov.task1;

public class SecondToHours {

    //    программа, которая конвертирует секунды в часы.
    //    Пример: на вход подается 3600, на выходе печатается 1 час.

    public static void main(String[] args) {

        // первый вариант реализации
        int getSeconds = 3600;
        int hours = getSeconds / 3600;
        int minutes = (getSeconds % 3600) / 60;
        String timeString = String.format("%02d Hour %02d Minutes", hours, minutes);
        System.out.println(timeString);

        // второй вариант реализации с входными параметрами
        int getSecondsParameter = Integer.parseInt(args[0]);
        int getHours = getSecondsParameter / 3600;
        int getMinutes = (getSecondsParameter % 3600) / 60;
        String timeStringParameter = String.format("%02d Hour %02d Minutes", getHours, getMinutes);
        System.out.println(timeStringParameter);

    }

}