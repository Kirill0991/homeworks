package ru.yarmonov.task14;

//Добавить в программу "Вендинговый автомат" журналирование событий, при этом стоит указать различные уровни логирования, как информационного уровня, так и предупреждения и ошибки.
//        Настроить сбор логов в файл.
//        Фреймворк логирования - на ваш выбор.

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.yarmonov.task14.drinks.DrinkType;
import java.util.Scanner;



public class App {

    static Logger logger = LoggerFactory.getLogger(App.class); ////////////////////////////// logger


    private static VendingMachine vm = new VendingMachine();

    public static void main(String[] args) {
        logger.info("начало работы программы");////////////////////////////// logger
        System.out.println("Наши напитки: ");
        for (String line : vm.getDrinkType()) {
            System.out.println(line);
        }

        Scanner scan = new Scanner(System.in);
        printHelp();
        while (scan.hasNext()) {
            try {
                String command = scan.next();
                logger.info("Пользователь ввел команду {}", command); ////////////////////////////// logger
                switch (command) {
                    case "add": {
                        int money = scan.nextInt();
                        processAddMoney(money);
                        break;
                    }
                    case "get": {
                        int key = scan.nextInt();
                        processGetDrink(key);
                        break;
                    }
                    case "exit": {
                        processEnd();
                        logger.trace("Закончили работу программы");////////////////////////////// logger
                        return;
                    }
                    default:
                        logger.warn("Ошибка ввода данных");////////////////////////////// logger
                        System.out.println("Не верный ввод");
                }
            } catch (MachineException exception) { ////////////////////////////// logger
                logger.warn("Исключение ", exception);
                System.out.println("Ошибка" + exception.getMessage());
                return;
            }

            scan.nextLine();
        }

        logger.info(" ");

    }

    private static void processEnd() {
        System.out.println("Спасибо!!!");
    }

    private static void processAddMoney(int money) {
        System.out.println("Текущий баланс: " + vm.addMoney(money));
    }

    private static void processGetDrink(int key) {
        DrinkType drinkType = vm.giveDrink(key);
        if (drinkType != null) {
            System.out.println(drinkType.getName() + "!");
        } else {
            logger.warn("Недостаточно средств");
            System.out.println("Недостаточно средств");
        }
    }

    private static void printHelp() {
        logger.info("Вывели список команд");
        System.out.println( "Введите 'add <сумма>' для добавления валюты" );
        System.out.println( "Введите 'get <код продукта>' для получения продукта" );
        System.out.println( "Введите 'exit' для выхода" );
    }
}
