package ru.yarmonov.task14.drinks;

public interface DrinkType {
    String getName();
    double getPrice();
}
