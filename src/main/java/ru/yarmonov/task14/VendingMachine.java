package ru.yarmonov.task14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yarmonov.task14.drinks.DrinkType;
import ru.yarmonov.task14.drinks.MenuDrinks;
import ru.yarmonov.task14.drinks.Product;

public class VendingMachine {

    private final static Logger logger = LoggerFactory.getLogger(VendingMachine.class); ////////////////////////////// logger

    private double money = 0;
    private Product[] drinks = new Product[]{
            new Product(MenuDrinks.LATTE, 100),
            new Product(MenuDrinks.AMERICANO, 100),
            new Product(MenuDrinks.CAPPUCINO, 100),
            new Product(MenuDrinks.BLACK_TEA, 80),
    };

    public double addMoney(double money) {
        this.money += money;
        return this.money;
    }

    public DrinkType giveDrink(int key){
        if (!isKeyValid(key)) {
            return null;
        }

        Product selected = drinks[key];
        if (!isMoneyEnough(selected)) {
            return null;
        }

        DrinkType drink = selected.take();
        money -= drink.getPrice();
        return drink;
    }

    public String[] getDrinkType() {
        String[] result = new String[drinks.length];
        for (int i = 0; i < drinks.length; i++) {
            result[i] = String.format("%d - %12s: %6.2f руб", i, drinks[i].getName(),drinks[i].getPrice());
        }
        return result;
    }

    private boolean isMoneyEnough(Product selected) {
        return money >= selected.getPrice();
    }

    private boolean isKeyValid(int key) {
        return key >=0 && key < drinks.length;
    }

}
