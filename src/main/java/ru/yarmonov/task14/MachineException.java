package ru.yarmonov.task14;

public class MachineException extends RuntimeException {
    public MachineException(String message) {
        super(message);
    }
}
