package ru.yarmonov.task29;

import java.util.HashMap;
import java.util.Map;

public class RemoveDuplicats {

    public static void removeTheDuplicates(Map<String, Person> map) {

        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> pair : copy.entrySet()) {
            int count = 0;
            Person value = pair.getValue();
            for (Map.Entry<String, Person> pair1 : copy.entrySet()) {
                if (pair1.getValue().equals(value)) {
                    count += 1;
                    if (count > 1) {
                        removeItemFromMapByValue(map, value);
                        break;
                    }
                }
            }
        }

    }


    public static void removeItemFromMapByValue(Map<String, Person> map, Person value) {
        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

}
