package ru.yarmonov.task29;

public class Person {
    private int age;
    private String surname;
    private String sex;

    public Person(int id, String firstName, String lastName) {
        this.age = id;
        this.surname = firstName;
        this.sex = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Person guest = (Person) obj;
        return age == guest.age
                && (surname == guest.surname
                || (surname != null && surname.equals(guest.getSurname()))) && (sex == guest.sex
                || (sex != null && sex.equals(guest.getSex())
        ));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((surname == null) ? 0 : surname.hashCode());
        result = prime * result + age;
        result = prime * result +
                ((sex == null) ? 0 : sex.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", surname='" + surname + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}