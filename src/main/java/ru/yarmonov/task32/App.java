package ru.yarmonov.task32;

public class App {

    /**
     * Программа для подсчета листовых узлов в двоичном дереве в Java.
     */

    public static void main(String[] args) {

        TreeNode rootNode = BinaryTreeLeafCount.createBinaryTree();
        System.out.println("Количество узлов: " + BinaryTreeLeafCount.getLeafCountOfBinaryTree(rootNode));

    }

}
