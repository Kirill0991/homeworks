package ru.yarmonov.task15;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Написать программу, которая будет создавать, переименовывать, копировать и удалять файл.
 * <p>
 * Написать рекурсивный обход всех файлов и подкаталогов внутри заданного каталога.
 * <p>
 * Дополнительное задание (необязательно): программа должна следить за глубиной рекурсии, сдвигая название файла/каталога на соответствующее количество пробелов.
 */


public class App {

    public static void main(String[] args) throws IOException {

        /**
         * Create
         */
        FileWorker fileWork = new FileWorker();
        File file = fileWork.createAndGetFile("test.txt");

        /**
         * Rename
         */
        //  Boolean b = fileWork.renameFile(file, "New_name.txt");


        /**
         * Remove
         */
        //  boolean r = fileWork.removeFile(file);


        /**
         * Copy
         */
        //  fileWork.copyFile(file, file.getAbsoluteFile());





        /**
         * Recursion
         */
        final String path = "C:\\Users\\Кирилл\\Desktop\\Home work Innopolis\\homework\\src\\main\\java\\ru\\yarmonov\\task15";
        ArrayList<String> res = new ArrayList<String>();

        try {
            listAll(path, res);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String s : res) {
            System.out.println(s);
        }

    }


    /**
     * Recursion method
     */
    private static void listAll(String path, ArrayList<String> res)
            throws IOException {
        File dir = new File(path);
        File[] list = dir.listFiles();

        for (File f : list) {
            if (f.isFile()) {
                res.add("Files: " + f.getCanonicalPath());
            } else {
                res.add("Directory: " + f.getCanonicalPath());
                listAll(f.getCanonicalPath(), res);
            }
        }
    }

}
