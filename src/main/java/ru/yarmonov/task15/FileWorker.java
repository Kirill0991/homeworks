package ru.yarmonov.task15;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileWorker {

    public File createAndGetFile(String fileName) {
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
                System.out.println("Файл " + file.getName() + " создан");
            }
            return file;
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        return null;
    }


    public boolean renameFile(File file, String newName) {

        final File newFile = new File(file.getAbsoluteFile().getParentFile().getAbsolutePath(), newName);

        if (file.exists() && !newFile.exists()) {
            if (file.renameTo(newFile)) {
                System.out.println("Файл переименован");
                return true;
            }
        }
        System.out.println("Файл не переименован");
        return false;
    }


    public void copyFile(File source, File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath());
        System.out.println(dest.toPath());
    }


    public boolean removeFile(File file) {

        final File removeFile = new File(file.getAbsoluteFile().getName());
        System.out.println(removeFile);
        if (removeFile.delete()) {
            System.out.println("Файл успешно удалён!");
        } else {
            System.out.println("Файл не существует");
        }
        return false;
    }


}

