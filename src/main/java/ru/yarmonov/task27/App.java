package ru.yarmonov.task27;


public class App {

    /**
     * Ряд Фибоначчи - это числовой ряд, в котором следующее число является суммой двух предыдущих чисел.
     * <p>
     * Например :
     * <p>
     * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 и т. д.
     * <p>
     * Есть два способа напечатать серии Фибоначчи.
     * <p>
     * Используя итерацию
     * <p>
     * Использование рекурсии
     * <p>
     * Ваша задача реализовать оба способа.
     */

    public static void main(String[] args) {

        int n = 10;
        int a = 0;
        int b = 1;
        for (int i = 2; i <= n; ++i) {
            int next = a + b;
            a = b;
            b = next;
            System.out.print(b + " ");
        }
        System.out.println(b);
        System.out.println(fibRecursion(n));

    }


    public static int fibRecursion(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return fibRecursion(n - 1) + fibRecursion(n - 2);
        }
    }

}
