package ru.yarmonov.task25;

import java.util.HashSet;
import java.util.Map;

/**
 * public boolean isUnique(Map<String, String> map);
 * Написать метод, который возвращает true, если в мапе нет двух и более одинаковых value, и false в противном случае.
 * Для пустой мапы метод возвращает true.
 * Например, для метода {Вася=Иванов, Петр=Петров, Виктор=Сидоров, Сергей=Савельев, Вадим=Викторов} метод вернет true,
 * а для {Вася=Иванов, Петр=Петров, Виктор=Иванов, Сергей=Савельев, Вадим=Петров} метод вернет false.
 */

public class App {

    public boolean isUnique(Map<String, String> map) {
        HashSet<String> set = new HashSet<>();
        for (String key : map.keySet()) {
            String value = map.get(key);
            if (set.contains(value))
                return false;
            set.add(value);
        }
        return true;
    }

}
