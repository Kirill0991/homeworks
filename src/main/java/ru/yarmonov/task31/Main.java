package ru.yarmonov.task31;

public class Main {

    /**
     * Палиндром - это слово, фраза, число или другая последовательность символов или элементов, которая читает то же самое вперед или назад.
     * <p>
     * Например: 12121 - палиндром, так как он читает то же самое вперед или назад. мадам тоже палиндром.
     * <p>
     * Требуется написать 2 реализации
     * <p>
     * 1. Перемещение с обоих концов строки одновременно
     * 2. Используя стандартные функции строки
     */

    public static void main(String[] args) {


        Palindrome pn = new Palindrome();

        if (pn.isPalindrome1("мадам")) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }

        if (pn.isPalindrome2("123131")) {
            System.out.println("Палиндром");
        } else {
            System.out.println("Не палиндром");
        }

    }


}
