package ru.yarmonov.task31;

public class Palindrome {

    public boolean isPalindrome1(String original) {
        int i = original.length() - 1;
        int j = 0;
        while (i > j) {
            if (original.charAt(i) != original.charAt(j)) {
                return false;
            }
            i--;
            j++;
        }
        return true;
    }

    public boolean isPalindrome2(String variable) {
        StringBuffer rev = new StringBuffer(variable).reverse();
        String strRev = rev.toString();
        if (variable.equalsIgnoreCase(strRev)) {
            return true;
        } else {
            return false;
        }
    }

}
