package ru.yarmonov.task3;

import java.util.Scanner;

public class InputStreamHandSalary {

    public static void main(String[] args) {

        Scanner InputStreamScan = new Scanner(System.in);
        int userHand = 0;
        int percent = 13;
        System.out.print("Заработная плата (до вычета): ");
        if (InputStreamScan.hasNextInt()) {
            userHand = InputStreamScan.nextInt();
            int totalUserHand = userHand * percent / 100;
            int a = userHand - totalUserHand;
            System.out.println("Заработная плата (после вычета): " + a + " р.");
        } else {
            System.out.println("Вы ввели не число");
        }

    }

}
