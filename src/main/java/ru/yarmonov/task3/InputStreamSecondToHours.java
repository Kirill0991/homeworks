package ru.yarmonov.task3;

import java.util.Scanner;

public class InputStreamSecondToHours {

    public static void main(String[] args) {

        Scanner InputStreamScan = new Scanner(System.in);
        int seconds = 0;
        System.out.print("Ввведите количество секунд: ");
        if (InputStreamScan.hasNextInt()) {
            seconds = InputStreamScan.nextInt();
            int userSeconds = seconds;
            int hours = userSeconds / 3600;
            int minutes = (userSeconds % 3600) / 60;
            String timeString = String.format("%02d Hour %02d Minutes", hours, minutes);
            System.out.println(timeString);
        } else {
            System.out.println("Вы ввели не число");
        }

    }

}
