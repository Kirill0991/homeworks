package ru.yarmonov.task24;

import java.util.ArrayList;

/**
 * Написать метод, который возвращает множество, в котором удалены все элементы четной длины из исходного множества.
 * <p>
 * public Set<String> removeEvenLength(Set<String> set);
 * <p>
 * Например, для множества ["foo", "buzz", "bar", "fork", "bort", "spoon", "!", "dude"] метод вернет ["foo", "bar", "spoon", "!"]
 */

public class App {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        list.add("foo");
        list.add("buzz");
        list.add("bar");
        list.add("fork");
        list.add("bort");
        list.add("spoon");
        list.add("!");
        list.add("dude");

        System.out.println("before = " + list);

        removeEvenLength(list);
        System.out.println("after = " + list);

    }


    public static void removeEvenLength(ArrayList<String> list) {
        int i = 0;
        while (i < list.size()) {
            String s = list.get(i);
            if (s.length() % 2 == 0) {
                list.remove(i);
            } else {
                i++;
            }
        }
    }

}
