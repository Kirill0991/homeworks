package ru.yarmonov.task21;

/**
 * Задача: Имеется массив, нужно переставить элементы массива в обратном порядке.
 * <p>
 * Задачу не зачитывать если использованы утильные методы класса Arrays.
 * Решением также не являются манупуляции с выводом массива.
 * Необходимо действительно перемещать элементы.
 * <p>
 * Вывести массив в консоль до и после вызова метода по реверсу массива
 */


public class App {

    public static void main(String[] args) {

        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();

        int n = a.length;
        int temp;

        for (int i = 0; i < n / 2; i++) {
            temp = a[n - i - 1];
            a[n - i - 1] = a[i];
            a[i] = temp;
        }
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }


}
