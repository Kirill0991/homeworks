package ru.yarmonov.task21;


/**
 * Дан двумерный массив. Задача – написать метод
 * <p>
 * public void toLeft() {}
 * <p>
 * 1.Пройти с 1-ой до последней строки
 * 2.Пройти с 1-го до предпоследнего элемента
 * 3.В текущую ячейку поместить значение следующей
 * 4.Последнему элементу присвоить 0
 * Так выглядит любая строка после преобразования данным методом
 * <p>
 * 2 3 4 5 6 7 8 9 10 0
 */

public class App2 {


    public static void main(String[] args) {

        int[][] m = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 6, 7, 8}};
        System.out.println("Before:");
        showArrays(m);
        totLeft(m);
        System.out.println("After:");
        showArrays(m);

    }

    public static void totLeft(int[][] m) {
        for (int i = 0; i < m.length; i++) {
            int temp = m[i][0];
            for (int j = m[i].length - 1; j >= 0; j--) {
                int val = m[i][j];
                m[i][j] = temp;
                temp = val;
            }
        }
    }

    public static void showArrays(int[][] m) {
        for (int[] s : m) {
            for (int k : s) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
    }


}
