package ru.yarmonov.task9.abstracts;

public class Man extends Person implements Runnable, Swimmable {

    private String name;

    public Man(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name + " бежит быстро");
    }

    @Override
    public void swim() {
        System.out.println(name + " плывет быстро");
    }
}
