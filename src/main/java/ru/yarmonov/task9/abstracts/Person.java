package ru.yarmonov.task9.abstracts;

abstract class Person implements Runnable, Swimmable {

    @Override
    public void run() {
        System.out.println("Бежит");
    }

    @Override
    public void swim() {
        System.out.println("Плывет");
    }
}
