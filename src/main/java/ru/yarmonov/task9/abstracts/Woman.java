package ru.yarmonov.task9.abstracts;

public class Woman extends Person {

    private String name;

    public Woman(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name + " бежит медленно...");
    }

    @Override
    public void swim() {
        System.out.println(name + " плывет медленно...");
    }

}
