package ru.yarmonov.task9.abstracts;

public interface Swimmable {
    void swim();
}
