package ru.yarmonov.task9.abstracts;

//3. Написать абстрактный класс Человек реализующий интерфейсы «бежать» и «плавать» (в каждом сделать 1-2 метода).
// Сделать несколько наследников этого класса с конкретной реализацией методов, которые объявлены в интерфейсах.

public class App {

    public static void main(String[] args) {

        Man man = new Man("Петя");
        man.swim();
        man.run();

        Woman woman = new Woman("Катя");
        woman.swim();
        woman.run();

   }

}