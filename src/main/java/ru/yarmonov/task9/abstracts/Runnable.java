package ru.yarmonov.task9.abstracts;

public interface Runnable {
    void run();
}
