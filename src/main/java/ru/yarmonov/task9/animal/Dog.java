package ru.yarmonov.task9.animal;

class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    public void names(){
        System.out.println("This is a " + super.getName());
    }

}
