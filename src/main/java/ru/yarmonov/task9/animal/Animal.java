package ru.yarmonov.task9.animal;

abstract class Animal {

    private String name;

    public String getName() {
        return name;
    }

    public Animal(String name) {
        this.name = name;
    }

    public abstract void names();

}
