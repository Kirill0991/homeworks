package ru.yarmonov.task9.animal;

//1. Написать абстрактный класс Animal с абстрактным методом getName.
// Сделать несколько классов животных, наследников Animal.
// Метод getName должен выводит название каждого животного.

public class App {

    public static void main(String[] args) {
        Cat cat = new Cat("Cat");
        cat.names();
        Dog dog = new Dog("Dog");
        dog.names();
    }

}
