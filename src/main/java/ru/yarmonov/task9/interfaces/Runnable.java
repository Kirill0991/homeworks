package ru.yarmonov.task9.interfaces;

public interface Runnable {
    void run();
}
