package ru.yarmonov.task9.interfaces;

public interface Flyable {
    void fly();
}
