package ru.yarmonov.task9.interfaces;

public interface Swimmable {
    void swim();
}
