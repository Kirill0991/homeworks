package ru.yarmonov.task9.interfaces;

public class Duck implements Swimmable, Flyable, Runnable {

    public void swim() {
        System.out.println("Утка плывет!");
    }

    public void fly() {
        System.out.println("Утка летит!");
    }

    public void run() {
        System.out.println("Утка бежит!");
    }

}
