package ru.yarmonov.task17;


public class Main {
    /**
     * Есть набор объектов типа "Книга". Каждая книга имеет название, автора, год издания. Можно больше, по желанию.
     * <p>
     * Программа должна уметь:
     * добавлять книгу в библиотеку.
     * показывать список книг в библиотеке.
     * восстанавливать содержимое библиотеки после перезапуска.
     * показывать соответствующее сообщение в случае ошибок.
     * <p>
     * Важно!
     * потоки обязательно должны закрываться
     * отсутствие файла на диске - не ошибка, а частный случай пустой библиотеки
     */

    public static void main(String[] args) {

        Book book = new Book();
        book.setYear(1867);
        book.setName("Война и мир");
        book.setNumberOfPage(1300);
        book.setBinding(true);

        Book book2 = new Book();
        book2.setYear(900);
        book2.setName("Анна Каренина");
        book2.setNumberOfPage(112);
        book2.setBinding(true);

        Book book3 = new Book();
        book3.setYear(1944);
        book3.setName("Скотный двор");
        book3.setNumberOfPage(112);
        book3.setBinding(true);

        Library.saveBooks(book, book2, book3);

        Library.loadBooks();


    }


}
