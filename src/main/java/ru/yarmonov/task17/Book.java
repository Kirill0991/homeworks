package ru.yarmonov.task17;

import java.io.Serializable;

public class Book implements Serializable {
    String name;
    int year;
    int numberOfPage;
    boolean binding;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumberOfPage() {
        return numberOfPage;
    }

    public void setNumberOfPage(int numberOfPage) {
        this.numberOfPage = numberOfPage;
    }

    public boolean isBinding() {
        return binding;
    }

    public void setBinding(boolean binding) {
        this.binding = binding;
    }


    @Override
    public String toString() {
        return String.format("Book [ name=%s, year=%d, numberofpage=%s ]", name, year, numberOfPage);
    }
}
