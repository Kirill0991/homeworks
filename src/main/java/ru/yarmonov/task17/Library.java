package ru.yarmonov.task17;

import java.io.*;
import java.util.ArrayList;

public class Library implements Serializable {

    ArrayList<Book> books;
    static String PATH_BOOKS = "C:\\Users\\Кирилл\\Desktop\\Home work Innopolis\\homework\\src\\main\\java\\ru\\yarmonov\\task17\\Books";

    public Library() {
        this.books = loadBooks();
    }

    public static ArrayList<Book> loadBooks() {
        ArrayList<Book> books = new ArrayList<>();

        File file = new File(PATH_BOOKS);

        for (File f : file.listFiles()) {
            if (!f.isDirectory()) {
                try (
                        FileInputStream fis = new FileInputStream(file);
                        ObjectInputStream ois = new ObjectInputStream(fis);
                ) {
                    books = (ArrayList<Book>) ois.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
        return books;

    }


    static void saveBooks(Book... books) {

        System.out.println("Сохранение книг: ");
        for (Book book : books) {
            System.out.println(book.toString());

            File file = new File(PATH_BOOKS, book.getName() + ".bin");
            try (
                    FileOutputStream fos = new FileOutputStream(file);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
            ) {
                oos.writeObject(book);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
