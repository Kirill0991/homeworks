package ru.yarmonov.task19;

import java.io.*;
import java.util.Scanner;


/**
 * Дан текстовый файл определенной структуры, в котором содержится информация о покупках.
 * <p>
 * Формат файла:
 * <p>
 * Название товара
 * количество
 * цена
 * <p>
 * Необходимо написать программу, которая выведет на экран чек, сформированный из этого файла. В чеке должна быть информация:
 * <p>
 * название товара:  цена Х кол-во = стоимость
 * <p>
 * В конце отчета вывести итоговую стоимость.
 * <p>
 * Пример входного файла и вывода прикрепляю к задаче
 */

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.printf("%-25s%5s%10s%14s%n", "Наименование", "Цена", "Кол-во", "Стоимость");
        System.out.println("======================================================");
        try (Scanner scanner = new Scanner(new File("data"))) {
            scanner.useDelimiter("~|\r\n|\n");
            float summ = 0;
            float total = 0;
            while (scanner.hasNext()) {
                String s = scanner.next();
                float i = scanner.nextFloat();
                float f = scanner.nextFloat();
                summ = i * f;
                total += summ;
                String v = String.format("%-25s%5.2f x %6.3f %13.2f\n", s, f, i, summ);
                System.out.print(v);
            }
            System.out.println("======================================================");
            System.out.printf("%5s%49.2f", "Итого", total);
        }


    }
}
