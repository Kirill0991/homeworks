package ru.yarmonov.task8.calc;

//2. Реализовать класс Calculator, который будет содержать статические методы для операций вычитания, сложения, умножения, деления и взятия процента от числа.
//        Класс должен работать как с целыми числами, так и с дробями.

import static ru.yarmonov.task8.calc.Calculator.calc;

public class App {

    public static void main(String[] args) {
        double num1 = GetNumber.getNumber();
        double num2 = GetNumber.getNumber();
        char operation = Calculator.getOperation();
        double result = Calculator.calc(num1, num2, operation);
        System.out.println("Результат: " + result);
    }

}
