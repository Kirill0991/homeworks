package ru.yarmonov.task8.counterObjects;

class CounterObject {

    private static int i;

    public CounterObject() {
        i++;
    }

    public static int getI() {
        return i;
    }
}
