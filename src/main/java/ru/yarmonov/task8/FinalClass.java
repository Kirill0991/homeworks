package ru.yarmonov.task8;

//1. Поэксперементировать с ключевым словом final

public class FinalClass {

    public static void main(String[] args) {

        final int[] array = {1,2,3,4,5};
        array[0] = 9;
//        array = new int[5]; //ошибка компиляции

    }



//    public final class String{
//    }
//
//    class SubString extends String{ //Ошибка компиляции
//    }


    public class SuperClass{
        public final void printReport(){
            System.out.println("Test");
        }
    }

    class SubClass extends SuperClass{
//        public void printReport(){  //Ошибка компиляции
//            System.out.println("NewTest");
//        }
    }

}
