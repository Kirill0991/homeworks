package ru.yarmonov.task8.contractAndAct;

//4. Реализовать два класса: договор и акт. В каждом сделать поля: номер, дата, список товаров (массив строк).
//        Написать класс со статическим методом конвертации договора в акт (на вход передавать договор, на выходе получаем акт).

public class App {

    public static void main(String[] args) {

        Contract contract = new Contract(9, 2020, new String[]{"a", "as", "asd"});
        Act act = Converter.convert(contract);
        System.out.println(act);

    }

}
