package ru.yarmonov.task8.contractAndAct;

import java.util.Arrays;

class Contract {

    private int number;
    private int date;
    private String[] products;

    Contract(int number, int date, String[] products) {
        this.number = number;
        this.date = date;
        this.products = products;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String[] getProducts() {
        return products;
    }

    public void setProducts(String[] products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "number=" + number +
                ", date=" + date +
                ", products=" + Arrays.toString(products) +
                '}';
    }

}
