package ru.yarmonov.task8.contractAndAct;

public class Converter {

    public static Act convert(Contract contract) {
        return new Act(contract.getNumber(), contract.getDate(), contract.getProducts());
    }

}
