package ru.yarmonov.task13;

public enum Food {
    CARROTS("Морковь"),
    APPLE("Яблоко"),
    PORRIDGE("Пюре"),
    JACKDANIELS("Сок");

    private final String name;

    Food(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
