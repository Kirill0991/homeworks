package ru.yarmonov.task13;

public class EatException extends Exception {
    public EatException(String message) {
        super(message);
    }
}
