package ru.yarmonov.task13;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;

public class Child {

    public String eat(String s) throws EatException {

//        public String eat(String s) throws EatException {
//
//            if ( Arrays.asList(Food.values()).stream().filter(s::equals).count() > 0) {
//                return "Спасибо, я поел";
//            }
//            throw new EatException("Мне это нельзя кушать");
//        }

        if (searchEat(s)) {
            return "съел " + s + " за обе щеки ";
        }
        throw new EatException("Мне это нельзя кушать");
    }

    private boolean searchEat(String eat) {
        for (Food food : Food.values()) {
            if (food.getName().equals(eat)) {
                return true;
            }
        }
        return false;
    }

}
