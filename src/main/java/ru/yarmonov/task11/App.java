package ru.yarmonov.task11;

//Напишите программу которая получает на вход некую строку , после она вызывает метод, заменяющий в строке все вхождения слова «бяка» на «вырезано цензурой» и выводит результат в консоль!

public class App {

    public static void main(String... args) {

        Filter filter = new Filter("Текстовая iphone11 с цензурным словом - бяка");
        filter.censoredFilter();

    }

}
