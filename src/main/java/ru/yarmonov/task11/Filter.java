package ru.yarmonov.task11;

public class Filter {

    private String a;
    private String[] censored = {"бяка", "мат", "iphone11"};

    public Filter(String a) {
        this.a = a;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "a='" + a + '\'' +
                '}';
    }


    public void censoredFilter() {

        String[] splitstring = a.split(" ");

        for (int k = 0; k < censored.length; k++) {
            for (int i = 0; i < splitstring.length; i++) {
                if (censored[k].equalsIgnoreCase(splitstring[i])) {
                    splitstring[i] = "[вырезано цензурой]";
                }
            }
        }

        for (int i = 0; i < splitstring.length; i++) {
            System.out.print(splitstring[i] + " ");
        }

    }
}
