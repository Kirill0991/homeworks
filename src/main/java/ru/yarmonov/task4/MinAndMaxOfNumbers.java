package ru.yarmonov.task4;


import java.util.Scanner;

//программа для поиска минимального из двух чисел

public class MinAndMaxOfNumbers {

    public static void main(String[] args) {
        boolean i = false;
        System.out.println("Введите первое число");
        Scanner scanner = new Scanner(System.in);
        int numberOne, numberTwo;

        while (!i) {
            if (scanner.hasNextInt()) {
                numberOne = scanner.nextInt();
                System.out.println("Введите второе число");
                numberTwo = scanner.nextInt();
                System.out.println("Меньшее число из вух чисел: " + Math.min(numberOne, numberTwo));
                System.out.println("---------------------------------------------");
                System.out.println("Введите первое число");
            } else {
                System.out.println("Вы ввели не число");
                i = true;
            }
        }
    }

}