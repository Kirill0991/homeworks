package ru.yarmonov.task4;

// программа, которая описывает введенное число. Отрицательное оно число или положительное (или нулевое), чётное или нечётное.

import java.util.Scanner;

public class NumberInformation {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число");
        boolean i = false;
        String exit = "выход";
        System.out.println("Если хотите выйти, введите слово 'выход'");

        while (!i) {
            String enter = scanner.nextLine();
            if (exit.equals(enter)) {
                i = true;
            } else {
                try {
                    int number;
                    number = Integer.parseInt(enter);
                    if (number < 0) {
                        System.out.println("Отрицательное");
                        Numbers(number);
                    } else if (number > 0) {
                        System.out.println("Положительное");
                        Numbers(number);
                    } else if (number == 0) {
                        System.out.println("Нулевое");
                        Numbers(number);
                    }
                } catch (NumberFormatException a) {
                    System.out.println("Вы ввели не число");
                }
            }
        }

    }

    static void Numbers(int number) {
        if (number % 2 == 0) {
            System.out.println("Четное");
        } else {
            System.out.println("Нечетное");
        }
    }

}
