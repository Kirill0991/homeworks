package ru.yarmonov.task4;

// программа для вывода на экран таблицы умножения.

public class MultiplicationTables {

    public static void main(String[] args) {

        int i = 1, n = 1;
        while (i <= 10) {
            System.out.printf("%4d", i);
            while (n <= 9) {
                n++;
                System.out.printf("%5d", i * n);
            }
            i++;
            n = 1;
            System.out.println("");
        }

        System.out.println("-------------------------------------------------------");

        for (i = 1; i <= 10; i++) {
            for (n = 1; n <= 10; n++) {
                int t = i * n;
                System.out.printf("%5d", + t);
            }
            System.out.println();
        }

    }

}
