package ru.yarmonov.task4;

import java.util.Random;
import java.util.Scanner;

public class HotAndCold {

    public static void main(String[] args) {

        int min = 0, max = 100, randomNumber = 0;
        boolean i = false;
        String exit = "выход";

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        randomNumber = random.nextInt((max - min) + 1) + min;
        System.out.println("Выберите число от 0 до " + max + ": ");

        System.out.println("Если хотите выйти, введите слово 'выход'");


        while (!i) {
            String enter = scanner.nextLine();
            if (exit.equals(enter)) {
                i = true;
            } else {
                try {
                    int num;
                    num = Integer.parseInt(enter);
                    if (num == randomNumber) {
                        System.out.println("Поздравляю!!!");
                        i = true;
                    } else {
                        System.out.println("Холодно");
                    }
                } catch (NumberFormatException a) {
                    System.out.println("Вы ввели не число");
                }
            }
        }

    }

}
