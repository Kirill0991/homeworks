package ru.yarmonov.task4;

// программа, которая выводит арифметическую или геометрическую прогрессию для N чисел

public class ArithmeticOrGeometricProgression {

    public static void main(String[] args) {

        // арифметическая
        int sum = 0;
        int n = 10;
        for (int i = 1; i <= n; i++) {
            System.out.println(sum += i);
        }

        // геометрическая
        int startNumber = 3;
        int multiplier = 3;
        int quantity = 6;
        System.out.println(startNumber);
        for (int i = 1; i <= quantity - 1; i++) {
            startNumber *= multiplier;
            System.out.println(startNumber);
        }


    }



}

