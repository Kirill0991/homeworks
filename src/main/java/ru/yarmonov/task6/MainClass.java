package ru.yarmonov.task6;

import ru.yarmonov.task6.PhonesInfo.*;
import ru.yarmonov.task6.phones.Phone;
import ru.yarmonov.task6.phones.VideoPhone;

public class MainClass {

    public static void main(String[] args) {
        AbstractPhone phone = new Phone(1984);
        AbstractPhone videoPhone=new VideoPhone(2018);
        User user = new User("Петя");
        user.callAnotherUser(111222333,phone);
        user.callAnotherUser(444555666,videoPhone);
    }

}
