package ru.yarmonov.task6.PhonesInfo;

// абстракция - выделить общее для всех объектов.

public abstract class AbstractPhone {
    private int year;

    public AbstractPhone(int year) {
        this.year = year;   // ссылка на текущий объект
    }

    public abstract void call(int outputNumber);

}
