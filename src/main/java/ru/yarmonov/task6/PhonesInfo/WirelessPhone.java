package ru.yarmonov.task6.PhonesInfo;

// наследование класса (extends)

public abstract class WirelessPhone extends AbstractPhone {
    private int hour;

    // конструктор
    public WirelessPhone(int year, int hour) {
        super(year);
        this.hour = hour;
    }

}
