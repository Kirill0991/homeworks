package ru.yarmonov.task6.phones;

// создание конструктора
// переопределение метода @Override

import ru.yarmonov.task6.PhonesInfo.AbstractPhone;

public class Phone extends AbstractPhone {

    public Phone(int year) {
        super(year);
    }

    @Override
    public void call(int outputNumber) {
        System.out.println("Вызываю номер " + outputNumber);
    }

}
