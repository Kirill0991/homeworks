package ru.yarmonov.task6.phones;

import ru.yarmonov.task6.PhonesInfo.AbstractPhone;

public class VideoPhone extends AbstractPhone {

    public VideoPhone(int year) {
        super(year);
    }
    @Override
    public void call(int outputNumber) {
        System.out.println("Подключаю видеоканал для абонента " + outputNumber );
    }
}
