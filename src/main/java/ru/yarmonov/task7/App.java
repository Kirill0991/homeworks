package ru.yarmonov.task7;

import ru.yarmonov.task7.VendingMachine;
import ru.yarmonov.task7.drinks.DrinkType;

import java.util.Scanner;

public class App {
    private static VendingMachine vm = new VendingMachine();

    public static void main(String[] args) {

        System.out.println("Наши напитки: ");
        for (String line : vm.getDrinkType()) {
            System.out.println(line);
        }

        Scanner scan = new Scanner(System.in);
        printHelp();
        while (scan.hasNext()) {
            String command = scan.next();
            switch (command) {
                case "add": {
                    int money = scan.nextInt();
                    processAddMoney(money);
                    break;
                }
                case "get": {
                    int key = scan.nextInt();
                    processGetDrink(key);
                    break;
                }
                case "exit": {
                    processEnd();
                    return;
                }
                default:
                    System.out.println("Не верный ввод");
            }
            scan.nextLine();
        }
    }

    private static void processEnd() {
        System.out.println("Спасибо!!!");
    }

    private static void processAddMoney(int money) {
        System.out.println("Текущий баланс: " + vm.addMoney(money));
    }

    private static void processGetDrink(int key) {
        DrinkType drinkType = vm.giveDrink(key);
        if (drinkType != null) {
            System.out.println(drinkType.getName() + "!");
        } else {
            System.out.println("Недостаточно средств");
        }
    }

    private static void printHelp() {
        System.out.println( "Введите 'add <сумма>' для добавления валюты" );
        System.out.println( "Введите 'get <код продукта>' для получения продукта" );
        System.out.println( "Введите 'exit' для выхода" );
    }
}
