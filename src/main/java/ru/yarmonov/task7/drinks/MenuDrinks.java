package ru.yarmonov.task7.drinks;

public enum MenuDrinks implements DrinkType{

    LATTE("Латте", 25),
    AMERICANO("Американо", 40),
    CAPPUCINO("Капучино", 100),
    BLACK_TEA("Черный чай", 25);

    private final String name;
    private final double price;

    MenuDrinks(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

}
