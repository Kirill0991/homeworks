package ru.yarmonov.task7.drinks;

public interface DrinkType {
    String getName();
    double getPrice();
}
