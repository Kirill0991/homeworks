package ru.yarmonov.additionalTask.filterInt;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        int[] array = {10, 2, 7, 3, 1, 4, 5, 9};
        for (int l = 0; l < array.length; l++) {
            int value = array[l];
            int i = l - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        System.out.println(Arrays.toString(array));
    }

}
