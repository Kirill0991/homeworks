package ru.yarmonov.task30;


/**
 * Напишите программу для поиска первого неповторяющегося символа в строке.
 * Например, первый неповторяющийся символ в «total» равен «o», а первый неповторяющийся символ в «teter» равен «r».
 */

public class App {

    public static void main(String[] args) {
        System.out.println(findFirstNonRepChar("total"));
        System.out.println(findFirstNonRepChar("teter"));
    }


    public static char findFirstNonRepChar(String text) {
        char currentChar = '\0';
        int len = text.length();
        for (int i = 0; i < len; i++) {
            currentChar = text.charAt(i);
            if ((i != 0) && (currentChar != text.charAt(i - 1)) && (i == text.lastIndexOf(currentChar))) {
                return currentChar;
            }
        }
        return currentChar;
    }

}
