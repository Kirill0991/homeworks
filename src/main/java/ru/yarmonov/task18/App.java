package ru.yarmonov.task18;

import java.io.*;

/**
 * Написать программу, которая копирует файл с одной кодировкой в файл с другой.
 */

public class App {

    public static void main(String args[]) throws Exception {
        FileInputStream fis = new FileInputStream("C:\\Users\\Кирилл\\Desktop\\Home work Innopolis\\homework\\src\\main\\java\\ru\\yarmonov\\task18\\test.txt");
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        Reader in = new BufferedReader(isr);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\Кирилл\\Desktop\\Home work Innopolis\\homework\\src\\main\\java\\ru\\yarmonov\\task18\\test2.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos, "windows-1251");
        Writer out = new BufferedWriter(osw);

        int ch;
        while ((ch = in.read()) > -1) {
            out.write(ch);
        }

        out.close();
        in.close();
    }

}
