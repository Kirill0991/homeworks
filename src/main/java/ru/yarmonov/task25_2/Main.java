package ru.yarmonov.task25_2;

/**
 * Реализовать класс корзины интернет магазина по следующему интерфейсу, используя реализацию мапы:
 * <p>
 * interface Basket {
 * <p>
 * void addProduct(String product, int quantity);
 * <p>
 * void removeProduct(String product);
 * <p>
 * void updateProductQuantity(String product, int quantity);
 * <p>
 * void clear();
 * <p>
 * List<String> getProducts();
 * <p>
 * int getProductQuantity(String product);
 * <p>
 * }
 */


public class Main {

    public static void main(String[] args) {
        MyBasket myBasket = new MyBasket();

        myBasket.addProduct("aaa", 4);
        myBasket.addProduct("bbb", 6);
        myBasket.addProduct("ccc", 7);

        for (String product : myBasket.getProducts()) {
            System.out.println(product + " " + myBasket.getProductQuantity(product));
        }

        myBasket.removeProduct("bbb");
        myBasket.updateProductQuantity("aaa", 5);

        for (String product : myBasket.getProducts()) {
            System.out.println(product + " " + myBasket.getProductQuantity(product));
        }
    }
}
