package ru.yarmonov.task25_2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MyBasket implements Basket {

    private Map<String, Integer> basketMap = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        basketMap.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        basketMap.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        basketMap.replace(product, quantity);
    }

    @Override
    public void clear() {
        basketMap.clear();
    }

    @Override
    public List<String> getProducts() {
        return basketMap
                .keySet()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    public int getProductQuantity(String product) {
        return basketMap.get(product);
    }
}
