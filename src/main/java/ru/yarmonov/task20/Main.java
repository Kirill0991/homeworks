package ru.yarmonov.task20;

//import org.json.JSONObject;


import java.io.*;
import java.net.URL;

import com.google.gson.Gson;

/**
 * Есть набор бесплатных сервисов в интернете, предоставляющих данные в формате JSON:
 * <p>
 * https://github.com/toddmotto/public-apis/blob/master/README.md
 * <p>
 * Выбрать любой сервис, какой больше нравится, и написать программу, которая с ним взаимодействует.
 * <p>
 * Класс сериализуемого объекта может содержать не все поля, а только 2-3 ключевых.
 * Например, для погоды достаточно показать диапазон температур.
 * Минимальное количество запросов к сервису - 1. Не обязательно реализовывать весь функционал, предоставляемый сервисом.
 * Достаточного одного любого запроса
 */

public class Main {
    public static void main(String[] ignored) throws Exception {

        URL url = new URL("https://httpbin.org/get?color=red&shape=oval");
        InputStreamReader reader = new InputStreamReader(url.openStream());
        JsonReader dto = new Gson().fromJson(reader, JsonReader.class);

        System.out.println(dto.headers);
        System.out.println(dto.args);
        System.out.println(dto.origin);
        System.out.println(dto.url);
    }

}