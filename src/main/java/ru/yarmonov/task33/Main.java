package ru.yarmonov.task33;

public class Main {

    /**
     * Вывести максимально встречающийся символ в строке.
     * <p>
     * Пример:
     * <p>
     * This is test message
     * <p>
     * Character: s has occurred maximum times in String: 5
     */

    public static void main(String[] args) {

        String word = "This is test message";
        int size = word.length();
        int max = 0;
        char maxChar = 'a';

        for (char x = 'A'; x <= 'z'; x++) {
            word = word.replace(String.valueOf(x), "");
            int newSize = word.length();
            if (size - newSize > max) {
                maxChar = x;
                max = size - newSize;
            }
            size = newSize;
        }

        System.out.println("Character: " + "'" + maxChar + "'" + " has occurred maximum times in String: " + "'" + max + "'");

    }


}
