package ru.yarmonov.task22;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {

    public int compare(Person customer1, Person customer2) {

        int NameCompare = customer1.getName().compareTo(customer2.getName());
        int AgeCompare = customer1.getAge().compareTo(customer2.getAge());

        if (NameCompare == 0) {
            return ((AgeCompare == 0) ? NameCompare : AgeCompare);
        } else {
            return NameCompare;
        }
    }


}
