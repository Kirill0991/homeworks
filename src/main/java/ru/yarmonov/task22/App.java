package ru.yarmonov.task22;

import java.util.*;

/**
 * Написать класс PersonSuperComparator,
 * <p>
 * который имплементит Comparator, но умеет сравнивать по двум параметрам , возраст и имя.
 * <p>
 * Класс Person теперь содержит два поля int age и String name
 */

public class App {

    public static void main(String[] args) {
        List<Person> per = new ArrayList<>();

        Person obj1 = new Person("Morpheus", 33);
        Person obj2 = new Person("Mr. Anderson", 23);
        Person obj3 = new Person("Trinity", 20);
        Person obj4 = new Person("Smith", 25);
        Person obj5 = new Person("Pifia", 19);

        per.add(obj1);
        per.add(obj2);
        per.add(obj3);
        per.add(obj4);
        per.add(obj5);

        Iterator<Person> custIterator = per.iterator();

        System.out.println("Before: ");
        while (custIterator.hasNext()) {
            System.out.println(custIterator.next());
        }

        Collections.sort(per, new PersonSuperComparator());

        System.out.println("After: ");
        for (Person customer : per) {
            System.out.println(customer);
        }

    }

}
